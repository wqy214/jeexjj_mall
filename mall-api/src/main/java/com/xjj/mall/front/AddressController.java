/****************************************************
 * Description: Controller for t_mall_address
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
	*  2018-09-13 zhanghejie Create File
**************************************************/
package com.xjj.mall.front;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xjj.framework.web.SpringControllerSupport;
import com.xjj.mall.common.pojo.Result;
import com.xjj.mall.common.utils.ResultUtil;
import com.xjj.mall.entity.AddressEntity;
import com.xjj.mall.service.AddressService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/front")
public class AddressController extends SpringControllerSupport{
	@Autowired
	private AddressService addressService;
	
    @RequestMapping(value = "/member/addressList",method = RequestMethod.POST)
    @ApiOperation(value = "获得所有收货地址")
    public Result<List<AddressEntity>> addressList(@RequestBody AddressEntity tbAddress){
        List<AddressEntity> list=addressService.getAddressList(tbAddress.getUserId());
        return new ResultUtil<List<AddressEntity>>().setData(list);
    }

    @RequestMapping(value = "/member/address",method = RequestMethod.POST)
    @ApiOperation(value = "通过id获得收货地址")
    public Result<AddressEntity> address(@RequestBody AddressEntity tbAddress){

    	AddressEntity address=addressService.getById(tbAddress.getId());
        return new ResultUtil<AddressEntity>().setData(address);
    }

    @RequestMapping(value = "/member/addAddress",method = RequestMethod.POST)
    @ApiOperation(value = "添加收货地址")
    public Result<Object> addAddress(@RequestBody AddressEntity tbAddress){

    	if(null == tbAddress.getId())
    	{
    		addressService.save(tbAddress);
    	}else
    	{
    		addressService.update(tbAddress);
    	}
        return new ResultUtil<Object>().setData(1);
    }

    @RequestMapping(value = "/member/updateAddress",method = RequestMethod.POST)
    @ApiOperation(value = "编辑收货地址")
    public Result<Object> updateAddress(@RequestBody AddressEntity tbAddress){

        addressService.update(tbAddress);
        return new ResultUtil<Object>().setData(1);
    }

    @RequestMapping(value = "/member/delAddress",method = RequestMethod.POST)
    @ApiOperation(value = "删除收货地址")
    public Result<Object> delAddress(@RequestBody AddressEntity tbAddress){
        addressService.delete(tbAddress);
        return new ResultUtil<Object>().setData(1);
    }
}