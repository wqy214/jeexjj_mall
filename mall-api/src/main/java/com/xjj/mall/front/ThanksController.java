/****************************************************
 * Description: Controller for t_mall_thanks
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
	*  2018-09-13 zhanghejie Create File
**************************************************/
package com.xjj.mall.front;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xjj.framework.web.SpringControllerSupport;
import com.xjj.mall.common.pojo.DataTablesResult;
import com.xjj.mall.common.pojo.Result;
import com.xjj.mall.common.utils.ResultUtil;
import com.xjj.mall.service.ThanksService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/front")
public class ThanksController extends SpringControllerSupport{


    @Autowired
    private ThanksService thanksService;

    @RequestMapping(value = "/member/thanks",method = RequestMethod.GET)
    @ApiOperation(value = "捐赠列表")
    public Result<DataTablesResult> getThanksList(@RequestParam(defaultValue = "1") int page,
                                                  @RequestParam(defaultValue = "20") int size){

        DataTablesResult result=thanksService.getThanksListByPage(page,size);
        return new ResultUtil<DataTablesResult>().setData(result);
    }

	
}

